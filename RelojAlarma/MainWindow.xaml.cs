﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace RelojAlarma
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Alarma AlarmaReloj;
        const string FileName = @"..\..\SavedAlarm.bin";

        public MainWindow()
        {
            AlarmaReloj = new Alarma();
            AlarmaReloj.HoraAlarma = "11:11";
            AlarmaReloj.AlarmaActiva = false;

            InitializeComponent();
            StartReloj();
            SaveAlarm();
            lblTime.Text = DateTime.Now.ToLongTimeString();

        }

        public void StartReloj()
        {
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += Timer_Tick;
            timer.Start();
        }

        void Timer_Tick(object sender, EventArgs e)
        {
            lblTime.Text = DateTime.Now.ToLongTimeString();

            if (AlarmaCheck.IsChecked)
            {
                if (lblTime.Text == TBAlarma.Text)
                {
                    MessageBox.Show("ESPABILA!");
                }
            }

        }
        private void SaveAlarm()
        {
            if (File.Exists("SavedAlarm.bin"))
            {
                Stream TestFileStream = File.OpenRead("SavedAlarm.bin");
                BinaryFormatter deserializer = new BinaryFormatter();
                AlarmaReloj = (Alarma)deserializer.Deserialize(TestFileStream);
                TestFileStream.Close();

                TBAlarma.Text = AlarmaReloj.HoraAlarma;
                AlarmaCheck.IsChecked = AlarmaReloj.AlarmaActiva;
            }
            else
            {
                TBAlarma.Text = "00:00:00";
            }
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

         
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Stream TestFileStream = File.Create(FileName);
            BinaryFormatter serializer = new BinaryFormatter();
            serializer.Serialize(TestFileStream, AlarmaReloj);
        }


        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Autor: Lluc Martinez Peregrina");
        }

        private void tbAlarma_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
           
            if (Regex.IsMatch(TBAlarma.Text, "^[012][0-9]:[0-5][0-9]$"))
            {
                AlarmaReloj.HoraAlarma = TBAlarma.Text;
            }

        }

        private void AlarmaCheck_Click(object sender, RoutedEventArgs e)
        {

        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {

        }
    }
}
